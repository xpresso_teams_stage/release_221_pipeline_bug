# Xpresso.AI
Xpresso AI is the python library for managing xpresso infrastructure. It contains multiple libraries which can help anyone use the xpresso infrastructure in a much better way

### Module Structure
- xpresso_ai_core:  It is a repository which will be used by the end users.
    - model
        - base_model
            - It is a base class which contains infer, train and validate method.
            - The end user will need to extend this class and implement the required functions
            - This is used to integrate with the Seldon
        - visualizer
            - Used to visualize the performance of models during training and inference
    - data
        - Contains the classes which can be used create, edit or transform the dataset
        - Can be used to update
    - devtools
        - Notebook
    - logging
        - xpresso_logging_handler
            - It is logging handler which uses the Xpresso ELK Stack
    - util
        - linux_util
        - config_util
        - string_util
        - file_util
        - unicode_util
- xpresso_ai_admin: It is the repository to be used for infrastructure management
    - controller
    - infra
        - packages
            - docker
            - kubernetes_master
            - kubernetes_node
            - base_vm
        - admin
            - package_installer_client

### Xpresso.Ai Classes

###### <b>Package Name</b>: xpresso_ai.controller

<b>Description</b>: contains classes for the Xpresso.ai controller, common to both client and server

1. <b>Authenticator</b>
    - authenticates user
    - login method checks in the loggedInUsers array to check if the user has logged in earlier, and has a valid access token. If not, it connects to the database, validates the login credentials, creates a new access token, stores it in loggedInUsers, and returns it to the calling method
    - logout method deletes the entry pertaining to the specified access token in loggedInUsers and returns true to the calling method

2. <b>UserLoginInfo</b> - data structure to store user ID, Access Token and Expiry date of the access token. The expiry date should be extended by the Authenticator for every call by a user

3. <b>BaseObject</b> - base class for all objects managed by the Controller. Implements common methods

4. <b>User</b> - represents an xpresso.ai user. CRUD operations on the database are implemented by the get, create, modify and delete methods

5. <b>Node</b> - represents a node (VM or physical server). CRUD operations on the database are implemented by the get, create, modify and delete methods

6. <b>NodeType</b>  - enumeration representing the different types of nodes

7. <b>Cluster</b> - represents a Kubernetes cluster. CRUD operations on the database are implemented by the get, create, modify and delete methods

8. <b>Project</b> - represents a customer project. CRUD operations on the database are implemented by the get, create, modify and delete methods. The deploy method enables deployment on a cluster

9. <b>UserRole</b> - data structure capturing the role played by a user in a project

10. <b>RoleType</b> - enumeration representing different types of roles users can play in a project

11. <b>VersionInfo</b> - data structure representing the version of a project / sub-project. Includes the version ID, and YAML file for deploying the project / sub-project to the Kubernetes cluster

###### <b>Package Name</b>: xpresso_ai.controller.server

<b>Description</b>: contains classes for the Xpresso.ai controller Server

1. <b>APIServer</b>- serves as the entry point for the Controller Server. Hands over the user request to the Authenticator, User, Node, Cluster or Project class, as appropriate

2. <b>ControllerServerException</b> - throws when any of the API requests has an error

###### <b>Package Name</b>: xpresso_ai.controller.client

<b>Description</b>: contains classes for the Xpresso.ai controller Client

1. <b>ControllerClient</b> - This class serves as a client to all the REST APIs provided by the contoller. It does not have a main method, and can be imported and used from within a Python (Jupyter) notebook

2. <b>XpressoCtl</b> - this is a CLI client for the Controller. It parses command line parameters, and passes requests to the ControllerClient object

3. <b>ControllerClientException</b> - throws when any of the client commands has an error

###### <b>Package Name</b>: xpresso_ai.test

<b>Description</b>: contains classes for the Xpresso.ai Test Framework

1. <b>TestManager</b>
    - This is the entry point class. It can be run manually, or as a cron job. It has access to a config file, which has all the components listed (e.g., "Jenkins", "Controller", etc.) It is invoked as follows TestManager -t <type> -c <optional components>. The "type" parameters can be one of:
(i) systest - typically run manually, this will create TestSuite objects for each component, and invoke the systest method of each of these
(ii) status - typically run as a low frequency (daily?) cron job, this will create TestSuite objects for each component, and invoke the status method of each of these
(iii) ping - typically run as a medium frequency (hourly?) cron job, this will create TestSuite objects for each component, and invoke the ping method of each of these
(iv) deploy - typically run as a low frequency (daily?) cron job, this will query the controller API to figure out the deployed projects / sub-projects, and run TestSuites for each of these - this will require the individual project developer to provide some test code to be run. Not yet sure how this will work, need to think about it. For example, we have overridden the "predict" method to provide "learn" and "validate" functionality. We can do the same for "ping", just to ensure that the container is responding.
    - The "component" parameter is optional. If specified, it indicates that tests are to be run on the specified component(s). If not, all components will be tested.
After executing the test suite(s), the TestManager will store the test results appropriately (log file / database / report)

2. <b>Interface (or Abstract Class) TestSuite</b> - this defines systest, status and ping methods as follows:
(i) TestResult[] systest () - each Suite will run a complete set of test cases - these should typically test complete functionality of the component. For example, the ControllerTestSuite should test all API calls of the controller (thus covering the "API testing" specified by Navin). The Suite should guarantee that the component is returned to the same state as it was in before the testing.
(ii) TestResult[] status() - each Suite will run a much smaller set of test cases, indicating whether its sub-components are up and running. For example, the Controller might test database connectivity, and return status accordingly.
(iii) boolean ping() - low-cost check to determine if the component is "alive"

3. <b>Interface (or abstract class) TestCase</b> - a specific test case - ideally this should be loaded from a JSON configuration file, indicating the test data, expected result, etc. For version 1, we can have the TestCases specified programmatically within a TestSuite implementation

4. <b>TestResult</b> - indicates the result of a test case. Can be a simple key-value pair, indicating the test description and the test result.


### Installation

##### Environment Setup

```
> export ROOT_FOLDER=$PWD
> export PYTHONPATH=${ROOT_FOLDER}
> export PYTHONUNBUFFERED=1
> export XPRESSO_CONFIG_PATH=config/common_stage.json
> export XPRESSO_PACKAGE_PATH=${ROOT_FOLDER}
```

##### Run
```
> pip install -r requirements_all.txt
> make run-local
```

### Building docker images

##### Base Xpresso Image
```
> make build SUFFIX=_base
```

##### Base Xpresso Core Image
```
> make build SUFFIX=_core
```

##### Base Xpresso EDA Image
```
> make build SUFFIX=_EDA
```

##### Base Xpresso EDA Image
```
> make build SUFFIX=_server
```
 
